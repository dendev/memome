import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Php',
    Svg: require('@site/static/img/logos/php.svg').default,
     to: '/php/intro',
    description: (
      <>
        Général, Patterns, Frameworks, Varnish, Prometheus
      </>
    ),
  },
  {
    title: 'Focus on What Matters',
    Svg: require('@site/static/img/logos/laravel.svg').default,
    description: (
      <>
        Docusaurus lets you focus on your docs, and we&apos;ll do the chores. Go
        ahead and move your docs into the <code>docs</code> directory.
      </>
    ),
  },
  {
    title: 'Powered by React',
    Svg: require('@site/static/img/logos/go.svg').default,
      to: '/php',
    description: (
      <>
        Extend or customize your website layout by reusing React. Docusaurus can
        be extended while reusing the same header and footer.
      </>
    ),
  },{
    title: 'Powered by React',
        to: '/php',
    Svg: require('@site/static/img/logos/sql.svg').default,
    description: (
      <>
        Extend or customize your website layout by reusing React. Docusaurus can
        be extended while reusing the same header and footer.
      </>
    ),
  },
];

function Feature({Svg, title, description, to}) {
  return (
      <div className={clsx('col col--4')}>
          <a href={to}>
              <div className="text--center">
                  <Svg className={styles.featureSvg} role="img" />
              </div>
              <div className="text--center padding-horiz--md">
                  <h3>{title}</h3>
                  <p>{description}</p>
              </div>
          </a>
      </div>
  );
}

export default function HomepageFeatures() {
    return (
        <section className={styles.features}>
            <div className="container">
                <div className="row">
                    {FeatureList.map((props, idx) => (
                        <Feature key={idx} {...props} />
                    ))}
                </div>
            </div>
        </section>
    );
}
